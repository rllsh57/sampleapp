package com.example.sampleapp.features.login

import android.view.View
import android.widget.Toast
import androidx.fragment.app.viewModels
import com.example.sampleapp.R
import com.example.sampleapp.arch.mvvm.MvvmFragment
import com.example.sampleapp.databinding.FragmentLoginBinding
import com.example.sampleapp.features.check_connection.ConnectionState
import com.example.sampleapp.features.navigation.AppNavigation
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LoginFragment : MvvmFragment<LoginEvent, AppNavigation, LoginState, LoginCommand, LoginViewModel, FragmentLoginBinding>() {

    override val layoutId = R.layout.fragment_login
    override val viewModel: LoginViewModel by viewModels()

    override fun bindView(view: View): FragmentLoginBinding {
        val binding = FragmentLoginBinding.bind(view)
        binding.buttonCheckConnection.setOnClickListener { sendEvent(LoginEvent.CheckConnectionClicked) }
        return binding
    }

    override fun renderState(state: LoginState) {
        val text = when (state.connectionState) {
            ConnectionState.Error -> getString(R.string.connection_error)
            ConnectionState.Connecting -> getString(R.string.connecting)
            ConnectionState.Established -> getString(R.string.connection_established)
            else -> ""
        }
        viewBinding.textConnectionState.text = text
    }

    override fun executeCommand(command: LoginCommand) {
        when (command) {
            is LoginCommand.SendMessage -> {
                Toast.makeText(requireContext(), R.string.message_sent, Toast.LENGTH_SHORT).show()
            }
        }
    }
}