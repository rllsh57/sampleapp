package com.example.sampleapp.features.navigation

import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.example.sampleapp.arch.navigation.NavigationHandler
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent

@Module
@InstallIn(FragmentComponent::class)
class AppNavigationModule {

    @Provides
    fun providesNavigationHandler(handler: AppNavigationHandler): NavigationHandler<AppNavigation> {
        return handler
    }

    @Provides
    fun provideNavController(fragment: Fragment): NavController {
        return fragment.findNavController()
    }
}