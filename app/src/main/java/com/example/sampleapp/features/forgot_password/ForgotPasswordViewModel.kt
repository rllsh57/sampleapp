package com.example.sampleapp.features.forgot_password

import com.example.sampleapp.arch.mvvm.MvvmViewModel
import com.example.sampleapp.features.check_connection.CheckConnectionInteractor
import com.example.sampleapp.features.check_connection.ConnectionState
import com.example.sampleapp.features.navigation.AppNavigation
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

@HiltViewModel
class ForgotPasswordViewModel @Inject constructor(
    private val interactor: CheckConnectionInteractor
) : MvvmViewModel<ForgotPasswordEvent, AppNavigation, ForgotPasswordState, ForgotPasswordCommand>() {

    override fun onStarted() {
        interactor.emitStates()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map { ForgotPasswordState(it) }
            .subscribe(state)
    }

    override fun handleEvent(event: ForgotPasswordEvent) {
        when (event) {
            ForgotPasswordEvent.CheckConnectionClicked -> {
                val connectionState = state.value?.connectionState
                interactor.checkConnection(connectionState) { sendCommand(ForgotPasswordCommand.SendMessage) }
            }
        }
    }
}


sealed class ForgotPasswordCommand {
    object SendMessage : ForgotPasswordCommand()
}

data class ForgotPasswordState(
    val connectionState: ConnectionState?
)

sealed class ForgotPasswordEvent {
    object CheckConnectionClicked : ForgotPasswordEvent()
}