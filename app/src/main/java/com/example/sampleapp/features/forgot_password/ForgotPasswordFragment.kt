package com.example.sampleapp.features.forgot_password

import android.view.View
import android.widget.Toast
import androidx.fragment.app.viewModels
import com.example.sampleapp.R
import com.example.sampleapp.arch.mvvm.MvvmFragment
import com.example.sampleapp.databinding.FragmentSignUpBinding
import com.example.sampleapp.features.check_connection.ConnectionState
import com.example.sampleapp.features.navigation.AppNavigation
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ForgotPasswordFragment : MvvmFragment<
        ForgotPasswordEvent,
        AppNavigation,
        ForgotPasswordState,
        ForgotPasswordCommand,
        ForgotPasswordViewModel,
        FragmentSignUpBinding>() {

    override val layoutId = R.layout.fragment_sign_up
    override val viewModel: ForgotPasswordViewModel by viewModels()

    override fun bindView(view: View): FragmentSignUpBinding {
        val binding = FragmentSignUpBinding.bind(view)
        binding.buttonCheckConnection.setOnClickListener { sendEvent(ForgotPasswordEvent.CheckConnectionClicked) }
        return binding
    }

    override fun renderState(state: ForgotPasswordState) {
        val text = when (state.connectionState) {
            ConnectionState.Error -> getString(R.string.connection_error)
            ConnectionState.Connecting -> getString(R.string.connecting)
            ConnectionState.Established -> getString(R.string.connection_established)
            else -> ""
        }
        viewBinding.textConnectionState.text = text
    }

    override fun executeCommand(command: ForgotPasswordCommand) {
        when (command) {
            is ForgotPasswordCommand.SendMessage -> {
                Toast.makeText(requireContext(), R.string.message_sent, Toast.LENGTH_SHORT).show()
            }
        }
    }
}