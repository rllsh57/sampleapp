package com.example.sampleapp.features.signup

import com.example.sampleapp.arch.mvvm.MvvmViewModel
import com.example.sampleapp.features.check_connection.CheckConnectionInteractor
import com.example.sampleapp.features.check_connection.ConnectionState
import com.example.sampleapp.features.navigation.AppNavigation
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

@HiltViewModel
class SignUpViewModel @Inject constructor(
    private val interactor: CheckConnectionInteractor
) : MvvmViewModel<SignUpEvent, AppNavigation, SignUpState, SignUpCommand>() {

    override fun onStarted() {
        interactor.emitStates()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map { SignUpState(it) }
            .subscribe(state)
    }

    override fun handleEvent(event: SignUpEvent) {
        when (event) {
            SignUpEvent.CheckConnectionClicked -> {
                val connectionState = state.value?.connectionState
                interactor.checkConnection(connectionState) { sendCommand(SignUpCommand.SendMessage) }
            }
        }
    }
}


sealed class SignUpCommand {
    object SendMessage : SignUpCommand()
}

data class SignUpState(
    val connectionState: ConnectionState?
)

sealed class SignUpEvent {
    object CheckConnectionClicked : SignUpEvent()
}