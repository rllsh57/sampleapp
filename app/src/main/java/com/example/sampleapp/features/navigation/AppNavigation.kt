package com.example.sampleapp.features.navigation

import androidx.navigation.NavController
import com.example.sampleapp.R
import com.example.sampleapp.arch.navigation.NavigationHandler
import dagger.hilt.android.scopes.FragmentScoped
import javax.inject.Inject

@FragmentScoped
class AppNavigationHandler @Inject constructor(
    private val navController: NavController
) : NavigationHandler<AppNavigation> {

    override fun handleNavigation(node: AppNavigation) {
        when (node) {
            AppNavigation.Login -> navController.navigate(R.id.login)
            AppNavigation.SignUp -> navController.navigate(R.id.sign_up)
        }
    }
}

sealed class AppNavigation {
    object Login : AppNavigation()
    object SignUp : AppNavigation()
}