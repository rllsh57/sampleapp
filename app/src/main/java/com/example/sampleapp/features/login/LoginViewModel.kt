package com.example.sampleapp.features.login

import com.example.sampleapp.arch.mvvm.MvvmViewModel
import com.example.sampleapp.features.check_connection.CheckConnectionInteractor
import com.example.sampleapp.features.check_connection.ConnectionState
import com.example.sampleapp.features.navigation.AppNavigation
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val interactor: CheckConnectionInteractor
) : MvvmViewModel<LoginEvent, AppNavigation, LoginState, LoginCommand>() {

    override fun onStarted() {
        interactor.emitStates()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map { LoginState(it) }
            .subscribe(state)
    }

    override fun handleEvent(event: LoginEvent) {
        when (event) {
            LoginEvent.CheckConnectionClicked -> {
                val connectionState = state.value?.connectionState
                interactor.checkConnection(connectionState) { sendCommand(LoginCommand.SendMessage) }
            }
        }
    }
}

sealed class LoginCommand {
    object SendMessage : LoginCommand()
}

data class LoginState(
    val connectionState: ConnectionState?
)

sealed class LoginEvent {
    object CheckConnectionClicked: LoginEvent()
}