package com.example.sampleapp.features.check_connection

import io.reactivex.rxjava3.core.Observable
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import kotlin.random.Random

class CheckConnectionInteractor @Inject constructor() {

    fun emitStates(): Observable<ConnectionState> {
        return Observable.interval(3, TimeUnit.SECONDS)
            .map { randomState() }
    }

    private fun randomState(): ConnectionState {
        val randomInt = Random.nextInt(3)
        return ConnectionState.values()[randomInt]
    }

    fun checkConnection(state: ConnectionState?, callback: () -> Unit) {
        if (state == ConnectionState.Established) {
            callback()
        }
    }
}

enum class ConnectionState {
    Error, Connecting, Established
}
