package com.example.sampleapp.arch.mvvm

import androidx.lifecycle.ViewModel
import io.reactivex.rxjava3.subjects.BehaviorSubject
import io.reactivex.rxjava3.subjects.PublishSubject

abstract class MvvmViewModel<UiEvent, NavigationEvent, State, Command> : ViewModel() {

    val state: BehaviorSubject<State> = BehaviorSubject.create()
    val commands: PublishSubject<Command> = PublishSubject.create()
    val uiEvents: PublishSubject<UiEvent> = PublishSubject.create()
    val navEvents: PublishSubject<NavigationEvent> = PublishSubject.create()

    open fun onStarted() {
    }

    fun subscribeEvents() {
        uiEvents.subscribe(::handleEvent)
    }

    fun sendCommand(command: Command) {
        commands.onNext(command)
    }

    fun navigate(destination: NavigationEvent) {
        navEvents.onNext(destination)
    }

    abstract fun handleEvent(event: UiEvent)
}