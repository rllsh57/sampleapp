package com.example.sampleapp.arch.navigation

interface NavigationHandler<Navigation> {
    fun handleNavigation(node: Navigation)
}