package com.example.sampleapp.arch.mvvm

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.toLiveData
import com.example.sampleapp.arch.navigation.NavigationHandler
import io.reactivex.rxjava3.core.BackpressureStrategy
import javax.inject.Inject

abstract class MvvmFragment<
        UiEvent,
        NavigationEvent,
        State,
        Command,
        ViewModel : MvvmViewModel<UiEvent, NavigationEvent, State, Command>,
        ViewBinding : androidx.viewbinding.ViewBinding
        > : Fragment() {

    abstract val layoutId: Int
    abstract val viewModel: ViewModel
    lateinit var viewBinding: ViewBinding

    @Inject
    lateinit var navigationHandler: NavigationHandler<NavigationEvent>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view = inflater.inflate(layoutId, container, false)
        viewBinding = bindView(view)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        observeState()
        observeNavigation()
        observeCommands()
        viewModel.subscribeEvents()
        viewModel.onStarted()
    }

    private fun observeState() {
        viewModel.state
            .toFlowable(BackpressureStrategy.ERROR)
            .doOnTerminate { Toast.makeText(requireContext(), "terminated", Toast.LENGTH_SHORT).show() }
            .toLiveData()
            .observe(viewLifecycleOwner, ::renderState)
    }

    private fun observeNavigation() {
        viewModel.navEvents
            .toFlowable(BackpressureStrategy.ERROR)
            .toLiveData()
            .observe(viewLifecycleOwner, ::handleNavigation)
    }

    private fun observeCommands() {
        viewModel.commands
            .toFlowable(BackpressureStrategy.ERROR)
            .toLiveData()
            .observe(viewLifecycleOwner, ::executeCommand)
    }

    fun sendEvent(event: UiEvent) {
        viewModel.uiEvents.onNext(event)
    }

    private fun handleNavigation(node: NavigationEvent) {
        navigationHandler.handleNavigation(node)
    }

    abstract fun bindView(view: View): ViewBinding

    abstract fun renderState(state: State)

    abstract fun executeCommand(command: Command)

}